// // An example of validation according to https://getbootstrap.com/docs/4.2/components/forms/#validation
// // adapted from https://www.codeply.com/go/mhkMGnGgZo/bootstrap-4-validation-example

// $("#example_form").submit(function (event) {

//   // make selected form variable
//   var vForm = $(this);

//   /*
//   If not valid prevent form submit
//   https://developer.mozilla.org/en-US/docs/Web/API/HTMLSelectElement/checkValidity
//   */
//   if (vForm[0].checkValidity() === false) {
//     event.preventDefault();
//     event.stopPropagation();
//   } else {

//     // Replace alert with ajax submit here...
//     alert("your form is valid and ready to send");

//   }

//   // Add bootstrap 4 was-validated classes to trigger validation messages
//   vForm.addClass('was-validated');

// });

// $(function () {
//   $('#example_form input[type="text"]').blur(function () {
//     //your validation code goes here
//     console.log($(this).val());
//   });
// });

$(function () {
  $('#example_form input[type="text"]').bind("keyup blur change", function (event) {
    var value = $(this).val();
    if ($(this).hasClass("hash-not-allowed-example"))
      if (isNotHashExample(value)) {
        $(this).removeClass("is-valid").addClass("is-invalid");
      }
    // else {
    //   $(this).removeClass("is-invalid").addClass("is-valid");
    // }
  });
});
//------------------------------------------------------------------------
function isNotHashExample(value) {
  // return (value === "#" ? true : false);
  return (value.includes('#') ? true : false);
}
//------------------------------------------------------------------------
