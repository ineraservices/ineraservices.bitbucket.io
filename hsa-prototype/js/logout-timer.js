/* jshint multistr: true */

// Simulate logout timer

// adapted from https://stackoverflow.com/questions/39911645/how-to-set-local-storage-on-timer

//localStorage.clear();
countDownTimer = function (sec) {
  if (typeof localStorage.getItem("autoLogoutInSec") !== 'undefined' && localStorage.getItem("autoLogoutInSec") != null) {
    sec = localStorage.getItem("autoLogoutInSec");
    console.log("Continue timer");
  } else {
    console.log("Start timer");
    $(document).alert_time_left();
  }

  if (sec === undefined) {
    sec = 10;
  }

  var countToZero = setInterval(function () {

    localStorage.setItem("autoLogoutInSec", sec);
    // document.getElementById("timer").innerHTML = sec;
    // console.log(sec);
    sec--;
    if (sec < 0) {
      clearInterval(countToZero);
      localStorage.removeItem("autoLogoutInSec");
      // If the count down is over, write some text
      // document.getElementById("demo").innerHTML = "EXPIRED";
      console.log("Session expired");
      window.location.href = '/' + firstLevelLocation + '/logged-out.html'; //redirect
    }
  }, 1000);
};

// Example usage: countDownTimer(5);

$(document).ajaxStop(function () {
  var loggedOutInSec = 5;
  $('#start-logout-timer-2min').click(function (e) {
    localStorage.removeItem("autoLogoutInSec");
    loggedOutInSec = 120;
  });
  if (localStorage.getItem("autoLogoutInSec") === null) {
    $('#start-logout-timer-2min,#start-logout-timer-5s').click(function (e) {
      if (localStorage.getItem("autoLogoutInSec") > 0) {
        $('#start-logout-timer-2min').append(' Stoppar...');
        clearInterval(heartBeat);
        localStorage.removeItem("autoLogoutInSec");
        // $('#top-nav-auto-log-out')
        //   .remove();
        location.reload();
      } else {
        countDownTimer(loggedOutInSec);
      }
      // Schedule the update to happen once every second
      var heartBeat = setInterval(function () {
        var timeLeft = localStorage.getItem("autoLogoutInSec");
        if (timeLeft < 1) {
          $('#start-logout-timer-2min').text('Automatisk utloggning (2 min)');
          // $('#top-nav-auto-log-out')
          //   .remove();
          // $('#top-nav-wrapper ul.navbar-nav.mr-auto')
          //   .append('<li class="nav-item" id="top-nav-auto-log-out"/>');
          // $('#top-nav-auto-log-out').click(function (e) {
          //   clearInterval(heartBeat);
          //   localStorage.removeItem("autoLogoutInSec");
          //   location.reload();
          //   e.stopPropagation();
          //   e.preventDefault();
          // });
        } else if (timeLeft === "120") {
          $('#session-continue').click(function (e) {
            clearInterval(heartBeat);
            localStorage.removeItem("autoLogoutInSec");
            location.reload();
            e.stopPropagation();
            e.preventDefault();
          });
        } else {
          $('#start-logout-timer-2min').text('Du loggas automatiskt ut om ' + timeLeft + ' s.');
          // $('#top-nav-auto-log-out').html('<a href="#" class="nav-link font-italic d-none d-xl-block">' + timeLeft + ' s. kvar<span class="d-block"><small>fortsätt sessionen<small><span></a>');
        }
        // If user is not on this page alert them to problem.
        if (!document.hasFocus()) {
          alert("Automatisk utloggning sker strax!");
        }
      }, 500);
      e.stopPropagation();
      e.preventDefault();
    });
  } else {
    // Continue counting
    countDownTimer(localStorage.getItem("autoLogoutInSec"));
    var heartBeat = setInterval(function () {
      var timeLeft = localStorage.getItem("autoLogoutInSec");
      if (timeLeft < 1) {
        // $('#top-nav-auto-log-out')
        //   .remove();
        $('#start-logout-timer-2min')
          .text('Utloggad!');
        return;
      } else {
        $('#start-logout-timer-2min').text('Du loggas automatiskt ut om ' + timeLeft + ' s.');
        // $('#top-nav-wrapper ul.navbar-nav.mr-auto').append('<li class="nav-item" id="top-nav-auto-log-out"/>');
        // $('#top-nav-auto-log-out').html('<a href="#" class="nav-link font-italic d-none d-xl-block">' + timeLeft + ' s. kvar<span class="d-block"><small>fortsätt sessionen<small><span></a>');
      }
      // $('#top-nav-auto-log-out').click(function (e) {
      //   clearInterval(heartBeat);
      //   localStorage.removeItem("autoLogoutInSec");
      //   location.reload();
      //   e.stopPropagation();
      //   e.preventDefault();
      // });
    }, 500);
    $('#start-logout-timer-2min').click(function (e) {
      clearInterval(heartBeat);
      localStorage.removeItem("autoLogoutInSec");
      location.reload();
      e.stopPropagation();
      e.preventDefault();
    });
  }
  $.fn.alert_time_left = function () {
    $('body').append('\
  <div class="modal" tabindex="-1" role="dialog" id="session-alert">\
    <div class="modal-dialog modal-dialog-centered" role="document">\
      <div class="modal-content">\
        <div class="modal-header">\
          <h5 class="modal-title">Du kommer snart att loggas ut automatiskt</h5>\
          <button type="button" class="close" data-dismiss="modal" aria-label="Stäng">\
            <span aria-hidden="true">&times;</span>\
          </button>\
        </div>\
        <div class="modal-footer">\
          <a href="' + '/' + firstLevelLocation + '/logged-out.html" class="btn btn-secondary">Logga ut nu</a>\
          <a href="#" class="btn btn-primary" data-dismiss="modal" id="session-continue">Fortsätt att vara inloggad</a>\
        </div>\
      </div>\
    </div>\
  </div>\
          ');
    $('#session-alert').modal('show');
  };

});
