/* jshint multistr: true */
// Prototyping - START

// Toggle rose notes by clicking header

$(function () {
  $('header').prepend(
    '<a href="#" class="prototype-subtle-message p-2 fixed-top offset-10 col-2 text-right" id="notes-toggler-hotspot">göm rosa anteckningar</a>'
  );
  $('#notes-toggler-hotspot').click(function (e) {
    $('.mock-message-note')
      .remove();
    e.preventDefault();
    $(this)
      .remove();
  });
  if (localStorage.getItem("hasRequirements") !== null) {
    $('body')
      .addClass('has-requirements');
    $('.requirements-note')
      .modal('show');
    $('.modal-backdrop')
      .addClass('modal-backdrop-requirements')
      .removeClass('modal-backdrop');
    $('body')
      .addClass('modal-open-requirements')
      .removeClass('modal-open');
    $('.requirements-note .close').click(function (e) {
      $('.modal-backdrop-requirements')
        .addClass('modal-backdrop')
        .removeClass('modal-backdrop-requirements');
      $('body')
        .removeClass('modal-open-requirements');
    });
  }
});

// find relative path to URL
var pathArray = window.location.pathname.split('/');
var firstLevelLocation = pathArray[1];
// console.log(firstLevelLocation);

var prototype_message;
$.get('/' + firstLevelLocation + '/assets/prototype-message.html', function (
  response
) {
  // prototype_message contains whatever that request returned
  prototype_message = response;
  $('#wrapper-simple-search').prepend(prototype_message);
  $('#prototype-message-placeholder').click(function () {
    $(this)
      .remove();
  });
  $('#prototype-message-placeholder .inner')
    .append('<h4 id="prototype-message-change-me-01"><a href="' + '/' + firstLevelLocation + '/pages/browse-alternatives.html">Olika varianter av bläddra</a></h4>')
    .append('<h4 id="prototype-message-change-me-02">Rapporter</h4>')
    .append('<h4 id="prototype-message-change-me-03"><a href="' + '/' + firstLevelLocation + '/detaljvy/">Detaljvyer</a></h4>')
    .append('<h4 id="prototype-message-change-me-04"><a href="' + '/' + firstLevelLocation + '/404.html">Felsida</a></h4>')
    .append('<h4 id="prototype-message-change-me-05"><a href="' + '/' + firstLevelLocation + '/pages/search-results.html?q=test">Sökresultatvy</a></h4>')
    .append('<h4 id="prototype-message-change-me-06"><a href="' + '/' + firstLevelLocation + '/detaljvy/person-pass/empty.html">Validering i formulär</a></h4>');

  $('#prototype-message-change-me-02')
    .wrapInner('<a href="#" class="mock-link" />')
    .on('click', function (e) {
      $('#sidebar-left, .sidebar-border')
        .toggleClass('active');
      $('#main-content-wrapper')
        .toggleClass('col-sm-7')
        .toggleClass('offset-sm-4')
        .toggleClass('col-sm-12');
      $('.sidebar-left-toggle>.toggle-sidebar')
        .toggleClass('opened closed');
      $('#rapportera-submenu')
        .toggleClass('show');
      $('#left-nav-item-04>.dropdown-toggle')
        .attr('aria-expanded', 'true');
      e.stopPropagation();
      e.preventDefault();
    });
  $('#prototype-message-change-me-02,#prototype-message-change-me-03')
    .on('click', function (e) {
      if (localStorage.getItem("isAdmin") == null) {
        // console.log('User has no privilegies');
        $(this).after('<i>(kräver administratörsrättigheter)</i>');
        $('#prototype-settings')
          .modal('show');
        $('.modal-backdrop')
          .removeClass("modal-backdrop");
        e.stopPropagation();
        e.preventDefault();
      }
    });
});

$("#omniboxSearch01").focus(function () {
  $.get(
    '/' + firstLevelLocation + '/assets/prototype-hints-home-page.html',
    function (response) {
      // prototype_settings contains whatever that request returned
      $('body').prepend(response);
    }
  );
});

var prototype_settings;
$.get(
  '/' + firstLevelLocation + '/assets/prototype-settings.html',
  function (response) {
    // prototype_settings contains whatever that request returned
    prototype_settings = response;
    $('body').prepend(prototype_settings);
  }
);

$('#toggle-prototype-settings').click(function (e) {
  $('#prototype-settings')
    .modal('show');
  $('.modal-backdrop')
    .removeClass("modal-backdrop");
  e.preventDefault();
});

$(function () {
  setTimeout(function () {
    $('body#wrapper-simple-search').click(function () {
      // $("#prototype-hint-placeholder").toggleClass('fade-2-visible fade-2-hidden');
      $('#prototype-hint-placeholder')
        .removeClass('fade-2-visible')
        .addClass('fade-2-hidden');
    });
  }, 1000);
  setTimeout(function () {
    // $("#prototype-hint-placeholder").toggleClass('fade-2-visible fade-2-hidden');
    $('#prototype-hint-placeholder')
      .removeClass('fade-2-visible')
      .addClass('fade-2-hidden');
    // $('#prototype-settings')
    //   .removeClass('fade-2-visible')
    //   .addClass('fade-2-hidden');
  }, 10000);
});

$(function () {
  setTimeout(function () {
    $('#prototype-message-placeholder')
      .toggleClass(
        'fade-2-visible fade-2-hidden'
      );
  }, 5000);
});

// Enable styling for user popover messages - START

$('.user-link').on('click', function () {
  $('.popover.show')
    .removeClass('user-message-popover')
    .addClass('user-message-popover');
});

$('.user-message').on('mouseover', function () {
  setTimeout(function () {
    $('.popover')
      .addClass('user-message-popover');
  }, 100);
});

$('.user-message').on('mouseleave', function () {
  $('.popover.show')
    .removeClass('user-message-popover');
});

$('.user-message').popover();

// Enable styling for user popover messages - STOP

// Use definition-bubble - START

$(function () {
  // $.fn.definition_bubbles = function () {
  var example_definition_bubble = '<div class="definition-bubble d-none"> \
<p>Detta är hjälptexten för attributet “Vårdenhetens ingående enheter”. HSA Förvaltning ansvarar för att ta fram differentierade hjälptexter på samtliga platser där sådana behövs.</p> \
<p>Enheter och funktioner som ingår i denna vårdenhet. Varje enhet/funktion får bara tillhöra en vårdenhet.</p> \
<p>Här kan du välja mellan de enheter/funktioner som är placerade direkt under vårdenheten, men också söka efter enheter/funktioner som är placerade i andra delar av organisationen.</p> \
<p>Läs mer om hur vårdgivar-/vårdenhetsorganisationen fungerar i <a href="https://www.inera.se/globalassets/tjanster/katalogtjanst-hsa/dokument/stodjande-dokument/handbok_for_hsa-administratorer.pdf" target="_blank">Handbok för HSA-administratörer</a>.</p> \
</div> \
';

  $('.has-definition').click(function () {

    if (
      !$(this)
      .find('.definition-bubble')
      .is(':visible')
    ) {
      $(this)
        .addClass('expanded')
        .append(example_definition_bubble)
        .find('.definition-bubble.d-none')
        .slideDown('fast')
        .removeClass('d-none')
        .addClass('show');
    } else {
      $(this)
        .removeClass('expanded')
        .find('.definition-bubble.show')
        .stop()
        .slideUp('fast');
    }

    // console.log(example_definition_bubble);
  });
  // };
  // $(document).definition_bubbles();
});

// Use definition-bubble - STOP

/* Create a popover for mock-links */

var mock_message_popover_options = {
  // template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"><h4>Fungerar inte i prototyp</h4><p>Kommer att fungera i skarp version!</p></div></div>',
  title: '<b>Prototypmeddelande</b>',
  content: '<p>Fungerar inte i prototyp. Kommer att fungera i skarp version.</p>',
  html: true,
  placement: 'auto'
};

$('.mock-message').popover(mock_message_popover_options);

/* Auto hide timer for prototype popover */

$('.mock-message').click(function () {
  setTimeout(function () {
    $('[data-original-title]')
      .popover('hide');
    //.removeClass('mock-message-popover');
  }, 5000);
});

// Enable styling for prototype messages - START
$('.mock-link').on('click', function () {
  $('.popover.show')
    .removeClass('mock-message-popover')
    .addClass('mock-message-popover');
});

$('.mock-message').on('mouseover', function () {
  setTimeout(function () {
    $('.popover')
      .addClass('mock-message-popover');
  }, 100);
});

$('.mock-message').on('mouseleave', function () {
  $('.popover.show')
    .removeClass('mock-message-popover');
});

/* Auto hide timer for prototype toast message */

// $(function () {
//   setTimeout(function () {
//     $(".mock-message-note.toast").fadeTo(500, 0).slideUp(500, function () {
//       $(this).remove();
//     });
//   }, 15000);
// });

// Enable styling for prototype messages - STOP

// Special note #001 - START
var mock_message_popover_001 = {
  // template: '<div class="popover" role="popover"><div class="popover-arrow"></div><div class="popover-inner"></div></div>',
  content: '<h4>Så här är det tänkt...</h4><p>Det är meningen att man ska kunna klicka fram den person, enhet eller organisation som är länkad från resultatlistan. Med andra ord ett utfällt träd snararare än att bara landa i toppen på "<a href="./browse.html">bläddra</a>".</p>',
  html: true,
  trigger: 'hover'
};
$('.mock-message-001').popover(mock_message_popover_001);

// Special note #001 - STOP

// Simulate 200+ hits limit - START
$(function () {
  $('.omnibox').on('input', '#omniboxSearch01', function () {
    if ($(this)
      .val().length && $(this)
      .val() === 'anna') {
      // alert('YES, ANNA IT IS!');
      $('form#hsa-simple-search')
        .attr('action', function (i, v) {
          return v.replace(/results\.html/g, 'results-200-limit.html');
        });
    }
  });
});

// Simulate 200+ hits limit STOP

// Simulate 195 hits + tabs - START
$('#wrapper-search-results .nav-link').click(function () {
  $('#wrapper-search-results .result-item')
    .addClass('d-block');
});
// Simulate 195 hits + tabs - STOP

// Simulate 0 hits - START
$('#omniboxSearch01').blur(function () {
  if ($(this)
    .val() == '') {
    // console.log('EMPTY SEARCH!');
    $('form')
      .attr('action', function (i, v) {
        return v.replace(/results\.html/g, 'results-empty.html');
      });
  }
});

// Simulate 0 hits - STOP

var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');

    if (sParameterName[0] === sParam) {
      return sParameterName[1] === undefined ?
        true :
        decodeURIComponent(sParameterName[1]);
    }
  }
};

var searchString = getUrlParameter('q');
// console.log(searchString);
$('#omniboxSearch01')
  .attr('placeholder', searchString);
$('#search-term-in-result-view')
  .html(searchString);
$('#search-term-in-result-view')
  .text(
    // Remove unwanted characters in result message
    $('#search-term-in-result-view')
    .text()
    .replace(/\+/g, ' ')
  );

// Search results – toggle attributes-short-view with a touch or click - START
$('#wrapper-search-results .result-item').on('click', function (e) {
  if (
    $(this)
    .hasClass('expanded')
  ) {
    $(this)
      .removeClass('expanded');
    $(this)
      .find('.attributes-short-view')
      .addClass('d-none');
  } else {
    $(this)
      .addClass('expanded')
      .find('i:first>svg[data-icon="caret-right"]')
      .removeClass('fa fa-caret-right')
      .addClass('fa fa-chevron-down');
    e.preventDefault();
    $(this)
      // Unfold selected detail view
      .find('>.path-to-item')
      .after($('.attributes-short-view')
        .removeClass('d-none'));
  }
  $('#expand-full-detail-view').on('click', function (e) {
    if (
      !$(this)
      .hasClass('expanded')
    ) {
      e.stopPropagation();
      e.preventDefault();
      $(this)
        .addClass('expanded')
        .html('<i class="fa fa-chevron-down" aria-hidden="true"></i>Visa färre detaljer');
      // Unfold selected detail view
      $('#full-detail-view')
        .removeClass('d-none');
    } else {
      e.stopPropagation();
      e.preventDefault();
      $('#full-detail-view')
        .addClass('d-none');
      $(this)
        .removeClass('expanded')
        .html('<i class="fa fa-caret-right" aria-hidden="true"></i>Visa fler detaljer');
    }
  });
});
// Search results – toggle attributes-short-view with a touch or click - STOP

$('.removable-item')
  .append(
    '<button class="ml-2 remove-me btn btn-secondary btn-sm"><span aria-hidden="true">×</span> Ta bort</button>'
  );

$('.removable-item>.remove-me').on('click', function (e) {
  var num = $(this)
    .parent()
    .parent()
    .parent('ul')
    .find('li').length;
  // console.log(num);
  if (num == 1) {
    $(this)
      .parent()
      .parent()
      .parent('ul')
      .addClass('now-an-empty-list');
  }
  $(this)
    .parent('.removable-item')
    .parent('li')
    .remove();
  $(this)
    .parent('.removable-item')
    .remove(); // double check there is nothing more to delete
  $('.now-an-empty-list')
    .html('<li><i>Uppgift saknas</i></li>');
  e.preventDefault();
});

$('.remove-me').on('click', function (e) {
  var num = $(this)
    .parent('td')
    .parent('tr')
    .parent('tbody')
    .find('tr').length;
  console.log(num);
  if (num == 1) {
    $(this)
      .parent('td')
      .parent('tr')
      .parent('tbody')
      .parent('table')
      .addClass('now-an-empty-table');
  }
  $(this)
    .parent('td')
    .parent('tr')
    .remove();
  $(this)
    .remove(); // double check there is nothing more to delete
  $('.now-an-empty-table')
    .html('<td><i>Uppgift saknas</i></td>');
  e.preventDefault();
});

// Prototyping - STOP

// popover setting - START

/* Where to show the message? */

$(function () {
  $('.general-message').popover({
    container: 'body'
  });
});

/* Dismiss all popovers by clicking outside */

$('html').on('click', function (e) {
  if (typeof $(e.target).data('original-title') == 'undefined') {
    $('[data-original-title]').popover('hide');
  }
});

/* Auto hide timer for general-message popover */

$('.general-message').click(function () {
  setTimeout(function () {
    $('[data-original-title]').popover('hide');
  }, 5000);
});

// popover setting - STOP

// toast general settings - START

$(function () {
  $('.toast.bs-toast-show').toast('show');
});

// toast general settings - STOP

$(function () {
  $('.remove_me').click(function (e) {
    $(this)
      .parent()
      .remove();
    e.stopPropagation();
    e.preventDefault();
  });
});

// Confirmation dialog - START

function confirmDialog(modal_confirm_message, modal_confirm_yes, modal_confirm_no) {
  modal_confirm_message = typeof modal_confirm_message !== 'undefined' ? modal_confirm_message : "Vad väljer du?";
  modal_confirm_yes = typeof modal_confirm_yes !== 'undefined' ? modal_confirm_yes : "Ja";
  modal_confirm_no = typeof modal_confirm_no !== 'undefined' ? modal_confirm_no : "Nej";

  var modal_block = '<div class="modal fade generic-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="btn-confirm" aria-hidden="true">' +
    ' <div class="modal-dialog modal-dialog-centered">' +
    ' <!-- Modal content-->' +
    ' <div class="modal-content">' +
    '   <div class="modal-body">' +
    '     <a href="#" class="close float-right" data-dismiss="modal">&times;</a>' +
    '     <div class="text-center pb-3">' + modal_confirm_message + '</div>' +
    '     <div class="d-flex justify-content-between no-gutters">' +
    '       <button class="btn btn-secondary btn-no" id="modal-answer-no">' + modal_confirm_no + '</button>' +
    '       <button class="btn btn-danger btn-yes" id="modal-answer-yes">' + modal_confirm_yes + '</button>' +
    '     </div>' +
    '   </div>' +
    ' </div>' +
    '</div>' +
    '</div>';
  $(modal_block).appendTo('body');

  modalConfirm = function (confirm) {

    $(".btn-confirm").on("click", function () {
      $(".generic-confirmation-modal")
        .modal('show');
    });

    $("#modal-answer-yes").on("click", function () {
      confirm(true);
      $(".generic-confirmation-modal")
        .modal('hide');
    });

    $("#modal-answer-no").on("click", function () {
      confirm(false);
      $(".generic-confirmation-modal")
        .modal('hide');
    });
  };

  modalConfirm(function (confirm) {
    if (confirm) {
      // Happens if answer is yes
      // console.log("I'm in!");
    } else {
      // Happens if answer is no
      // console.log("No way!");
    }
  });
}

// Usage:
/*
// no custom values
confirmDialog();
// with your own variables
confirmDialog("Is this working?", "Looks like it", "It is broken");
*/

// Confirmation dialog - STOP

// Toggle disable
// Person
$('#c1_person').click(function () {
  $('#givenName,#surname,#title,#personalId,#passportId')
    .prop('disabled', function (i, v) {
      return !v;
    });
  $('.disabled-icon')
    .remove();
  var disabled_elements = $(':disabled');
  $(disabled_elements)
    .each(function (index, element) {
      $(this).after(
        '<i class="fa fa-lock fa-lg disabled-icon" aria-hidden="true"/>'
      );
    });
  $('#c1')
    .toggleClass('d-none');
});

// Unit
$('#c2_organization').click(function () {
  $('#businessClassificationCode,#localityName,#telephoneNumber,#streetAddress,#eMail')
    .prop('disabled', function (i, v) {
      return !v;
    });
  $('.disabled-icon')
    .remove();
  var disabled_elements = $(':disabled');
  $(disabled_elements)
    .each(function (index, element) {
      $(this).after(
        '<i class="fa fa-lock fa-lg disabled-icon" aria-hidden="true"></i>'
      );
    });
  $('#c2')
    .toggleClass('d-none');
});

// Commission
$('#c3_commission').click(function () {
  $('#hsaCommissionPurpose')
    .prop('disabled', function (i, v) {
      return !v;
    });
  $('.disabled-icon')
    .remove();
  var disabled_elements = $(':disabled');
  $(disabled_elements)
    .each(function (index, element) {
      $(this).after(
        '<i class="fa fa-lock fa-lg disabled-icon" aria-hidden="true"/>'
      );
    });
  // $('#hsaCommissionPurpose').next().toggleClass('disabled');
  $('#c3')
    .toggleClass('d-none');
});

// Toggle any table column - START

$("input:checkbox:not(:checked)")
  .each(function () {
    var column = "table ." + $(this)
      .attr("name");
    $(column).hide();
  });

$("input:checkbox").click(function () {
  var column = "table ." + $(this)
    .attr("name");
  $(column).toggle();
});
// <input type="checkbox" name="some-class" checked="checked"> hides td.some-class

// Toggle any table column - STOP

// Select all checkboxes in a table


// Exact search
$('#exact-search-filter-check').click(function () {
  $('#filter-exact-search')
    .toggleClass('d-none');
});

// Detailed search - toggle disabled fields - STOP

$('.toggle-card-details').click(function (e) {
  // $('#more-detailed,#less-detailed').toggleClass('d-none');
  e.preventDefault();
});

$('#more-detailed .toggle-card-details').click(function (e) {
  $('#more-detailed,#less-detailed')
    .toggleClass('d-none');
  e.preventDefault();
});

// Detail view - toggle disabled objects - START

$('#hiddenObject').click(function (e) {
  if ($(this).is(":checked")) {
    // alert("Checkbox being checked.");
    $(".generic-confirmation-modal")
      .remove(); // Remove the standard modal and create a new one
    confirmDialog(
      "<div class='text-left'><p>Dölj objekt används för att inaktivera objekt i katalogen, t.ex. personer som ännu inte har börjat sin anställning eller vars anställning har upphört men som av olika anledningar ännu inte kan tas bort ur HSA.</p><ul><li>Inaktiverade objekt syns inte i t.ex. <i>Sök i HSA</i> eller på <i>1177 Vårdguiden</i>, och inte heller för administratörer som inte har rätt att administrera objektet</li><li>Inaktiverade personer kan inte logga in i tjänster som hämtar behörighetsgrundande information från HSA, t.ex. Pascal eller 1177 Vårdguidens e-tjänster</li><li>Personer kan heller inte logga in i tjänster som hämtar behörighetsgrundande information från HSA om den vårdgivare, vårdenhet eller enhet där uppdraget är placerat - eller uppdraget i sig - är inaktiverat</li><li>Däremot är inaktiverade objekt synliga i SITHS så att SITHS-kort/-certifikat kan administreras</li></ul><div>", "Dölj objekt", "Avbryt"
    );
    setTimeout(function () {
      $(".generic-confirmation-modal")
        .modal('show');
      modalConfirm(function (confirm) {
        if (confirm) {
          // console.log("yes, yes, yes");
          $('section[role="main"]>.container-fluid')
            .addClass('zig-zag');
          $('#hiddenObjectLabelHide')
            .addClass('d-none');
          $('#hiddenObjectLabelShow')
            .removeClass('d-none');
        } else {
          // console.log("no, no, no");
          $('#hiddenObject')
            .prop("checked", false);
        }
      });
    }, 100);
  } else if ($(this).is(":not(:checked)")) {
    // alert("Checkbox being unchecked.");
    $('section[role="main"]>.container-fluid')
      .removeClass('zig-zag');
    $('#hiddenObjectLabelHide')
      .removeClass('d-none');
    $('#hiddenObjectLabelShow')
      .addClass('d-none');
  }
  // e.preventDefault();
});

// Get personal identity number - START
$('#update-personal-identity-number-btn').click(function (e) {
  // console.log('change-personal-identity-number clicked');
  $(".generic-confirmation-modal")
    .remove(); // Remove the standard modal and create a new one
  confirmDialog(
    "<div class='text-left'><p>Om denna person har uppgifter om ett nytt person-id (personnummer eller samordningsnummer) i befolkningsregistret kommer detta nya person-id att ersätta tidigare person-id. Eventuella andra namnuppgifter kopplade till det nya person-id:t kommer att också att hämtas och vid behov ersätta tidigare namnuppgifter. HSA-id och övrig information påverkas inte.</p><div>", "Hämta nytt person-id", "Avbryt"
  );
  setTimeout(function () {
    $(".generic-confirmation-modal")
      .modal('show');
    modalConfirm(function (confirm) {
      if (confirm) {
        // console.log("yes, yes, yes");
        $(document).get_external_source_modal();
      } else {
        // console.log("no, no, no");
      }
    });
  }, 100);
});
// Get personal identity number - STOP

// Edit personal identity number - START
$('#edit-personal-identity-number-btn').click(function (e) {
  // console.log('change-personal-identity-number clicked');
  $(".generic-confirmation-modal")
    .remove(); // Remove the standard modal and create a new one
  confirmDialog(
    "<div class='text-left'><p>Om denna person har fått ett person-id (personnummer eller samordningsnummer) i befolkningsregistret kommer detta nya person-id att ersätta tidigare passuppgifter. Namnuppgifter kopplade till det nya person-id:t kommer att också att hämtas och vid behov ersätta tidigare namnuppgifter. HSA-id och övrig information påverkas inte.</p><label for='editPersonalIdNumber' class='mb-3'>Ange person-id för personen</label><input type='text' class='form-control mb-3' id='editPersonalIdNumber' placeholder='T.ex. 191212121212'><div>", "Verifiera person-id och hämta uppgifter", "Avbryt"
  );
  setTimeout(function () {
    $(".generic-confirmation-modal")
      .modal('show');
    modalConfirm(function (confirm) {
      if (confirm) {
        // console.log("yes, yes, yes");
        $(document).get_external_source_modal();
        setTimeout(function () {
          window.location = '../person-befolkningsregister/index.html';
        }, 2000);
      } else {
        // console.log("no, no, no");
      }
    });
  }, 100);
});
// Edit personal identity number - STOP

$('#hsaConfidentialPerson').click(function () {
  var radioValue = $("input[name='hsaConfidentialPerson']:checked")
    .val();
  // if (radioValue) {
  //   alert("Status - " + radioValue);
  // }
  if (radioValue === 'hsaConfidentialPersonHideButEnableEdit') {
    $('#object-container')
      .addClass('d-none')
      .removeClass('d-none');
  } else {
    $('#object-container')
      .removeClass('d-none')
      .addClass('d-none');
  }
});

$('#hsaConfidentialPersonConfirmedShow').on('click', function () {
  $('#object-container')
    .addClass('d-none')
    .removeClass('d-none');
});

$('#hsaConfidentialPersonConfirmedHide, #hsaConfidentialPersonModal .close').on(
  'click',
  function () {
    $('input:radio[name="hsaConfidentialPerson"]')
      .filter('[value="hsaConfidentialPersonHideForAll"]')
      .prop('checked', true);
  }
);

// Detail view - toggle disabled objects - STOP

// Bookmarkable headlines

$(function () {
  $(
      "section[role='main'] h1, section[role='main'] h2, section[role='main'] h3, section[role='main'] h4, section[role='main'] h5, section[role='main'] h6"
    )
    .each(function (i) {
      var heading = $(this);
      var headingtext = heading
        .text()
        .toLowerCase()
        .trim()
        .replace(/[\.,-\/#!?$%\^&\*;:{}=\-_`~()]/g, '')
        .replace(/ /g, '-')
        .replace(/[^\x00-\x7F]/g, '');
      // headingtext = encodeURIComponent(headingtext);
      heading.attr('id', headingtext);
    });
});

$('#logout-icon').on('click', function (e) {
  window.location.href = '/' + firstLevelLocation + '/logged-out.html'; //redirect
  e.stopPropagation();
  e.preventDefault();
});

// View link in tab navigation

$('.nav-tabs .view-link')
  .prepend(
    '<i class="fa fa-thumbtack" aria-hidden="true"></i>'
  );

// Edit link in tab navigation

$('.nav-tabs .edit-link')
  .prepend(
    '<i class="fa fa-pen" aria-hidden="true"></i>'
  );

// Delete link in tab navigation

$('.nav-tabs .delete-link,.nav-tabs .delete-btn')
  .prepend(
    '<i class="fa fa-trash" aria-hidden="true"></i>'
  );

$('#delete-example-001').on('click', function () {
  confirmDialog("Funktionen <i>IT-avdelningen</i> kommer nu att tas bort", "Radera", "Avbryt");
  modalConfirm(function (confirm) {
    if (confirm) {
      // Happens if answer is yes
      // console.log("Sure!");
      $('#wrapper-detail-view')
        .html('\
      <div class="container h-100">\
        <div class="row h-100 justify-content-center align-items-center">\
          <div class="col-md-9">\
            <h2>Funktionen <i>IT-avdelningen</i> är nu <i>borttagen</i> ur katalogen.</h2>\
            <h3><a href="../../">Till startsidan</a></h3>\
          </div>\
        </div>\
      </div>\
      ');
    } else {
      // Happens if answer is no
      // console.log("No way!");
      // We need to clean up here
      setTimeout(function () {
        $(".generic-confirmation-modal")
          .remove();
      }, 500);
    }
  });
  $(".generic-confirmation-modal")
    .modal('show');
});

$('#delete-example-002').on('click', function () {
  confirmDialog("Enheten <i>Blekingesjukhuset Karlskrona</i>  kan inte tas bort eftersom den har underliggande objekt.");
  $('#modal-answer-no')
    .remove();
  $('#modal-answer-yes')
    .remove();
  $(".generic-confirmation-modal")
    .modal('show');
  setTimeout(function () {
    $(".generic-confirmation-modal")
      .modal('hide');
  }, 5000);
  setTimeout(function () {
    $(".generic-confirmation-modal")
      .remove();
  }, 5500);

});

$(function () {
  if ($('.selectpicker').length) {
    $('.selectpicker').selectpicker({
      noneSelectedText: 'Välj något' // by this default 'Nothing selected' --> will change to whatever you enter here
    });
  }
});

$('.toggle-sidebar').on('click', function () {
  $('#sidebar-left, .sidebar-border')
    .toggleClass('active');
  $('#main-content-wrapper')
    .toggleClass('col-sm-7')
    .toggleClass('offset-sm-4')
    .toggleClass('col-sm-12');
  $('.sidebar-left-toggle>.toggle-sidebar')
    .toggleClass('opened closed');
});
$('.toggle-filters').on('click', function () {
  $('#filter-submenu')
    .toggleClass('show');
  $('#left-nav-item-01>.dropdown-toggle')
    .attr('aria-expanded', 'true');
});
$('.toggle-manage').on('click', function () {
  $('#administrera-submenu')
    .toggleClass('show');
  $('#left-nav-item-02>.dropdown-toggle')
    .attr('aria-expanded', 'true');
});
$('.toggle-reporting').on('click', function () {
  $('#rapportera-submenu')
    .toggleClass('show');
  $('#left-nav-item-04>.dropdown-toggle')
    .attr('aria-expanded', 'true');
});
$('.toggle-links').on('click', function () {
  $('#lankar-submenu')
    .toggleClass('show');
  $('#left-nav-item-05>.dropdown-toggle')
    .attr('aria-expanded', 'true');
});

// Control section - START

// Control section - STOP

// Detail view - START

/*
$('#wrapper-detail-view.preview .card .card-body:not(:has(".card-text"))')
  .parent()
  .hide();
*/

/*
$('#wrapper-detail-view .card .card-body:not(:has(".card-text")')
  .children()
  .css('border-bottom', '1px solid #e9ecef');
$('.card-text')
.css('border-bottom', 'none');
*/

/*
$('.preview-card')
  .addClass('card')
  .children('div:first-child')
  .addClass('card-body row')
  .children('div:first-child')
  .addClass('card-text col-sm-6 text-right')

$('.preview-card>div>div:nth-child(2)')
  .addClass('card-text col-sm-6 font-weight-bold')
*/

$('.preview-card')
  .addClass('card')
  .each(function () {
    $(this)
      .children().wrapAll("<div class='card-body form-row'></div>");
  });
$(".preview-card>div>div:nth-child(2n-1)")
  .addClass('card-text col-sm-6 text-right');
$(".preview-card>div>div:nth-child(2n)")
  .addClass('card-text col-sm-6 font-weight-bold');

// Detail view - STOP

// Treeview - START

/*
$('#expand-all-branches').on('click', function () {
  $('ul.tree-view i.fa-caret-right')
    .removeClass('fa fa-caret-right')
    .addClass('fa fa-chevron-down');
  $('ul.tree-view ul').show();
  $('#collapse-all-branches')
    .removeClass('d-none')
    .addClass('d-block');
  $(this)
    .removeClass('d-block')
    .addClass('d-none');
  return false;
});

$('#collapse-all-branches').on('click', function () {
  $('ul.tree-view i.fa-chevron-down')
    .removeClass('fa fa-chevron-down')
    .addClass('fa fa-caret-right');
  $('ul.tree-view ul').hide();
  $('#expand-all-branches')
    .removeClass('d-none')
    .addClass('d-block');
  $(this)
    .removeClass('d-block')
    .addClass('d-none');
  return false;
});
*/

$('#collapse-all-branches').on('click', function () {
  $('ul.tree-view i.fa-chevron-down')
    .removeClass('fa fa-chevron-down')
    .addClass('fa fa-caret-right');
  $('ul.tree-view ul').hide();
  $('.attributes-short-view')
    .addClass('d-none');
  $('#expand-full-detail-view')
    .removeClass('expanded');
  $('ul.tree-view')
    .find('i>svg[data-icon="chevron-down"]')
    .removeClass('fa fa-chevron-down')
    .addClass('fa fa-caret-right');
  $(this)
    .parent()
    .removeClass('d-block')
    .addClass('d-none');
  $('#no-of-objects-in-tree')
    .parent()
    .removeClass('col-xl-3')
    .addClass('col-xl-6');

  return false;
});

// modal--create-new-object - START

$('#create-object-here').on('click', function (e) {
  $("#modal--create-new-object")
    .modal("show");
  e.stopPropagation();
  e.preventDefault();
});

$('#create-new-person-pass').on('click', function (e) {
  $("[id^='modal--create-new-']")
    .modal('hide');
  $('#modal--create-new-person-pass')
    .modal('show');
  e.stopPropagation();
  e.preventDefault();
});

$('#create-new-person-befolkningsregister').on('click', function (e) {
  $("[id^='modal--create-new-']")
    .modal('hide');
  $('#modal--create-new-person-befolkningsregister')
    .modal('show');
  e.stopPropagation();
  e.preventDefault();
});

$('#create-new-vardmedarbetaruppdrag').on('click', function (e) {
  $("[id^='modal--create-new-']")
    .modal('hide');
  $('#modal--create-new-vardmedarbetaruppdrag')
    .modal('show');
  e.stopPropagation();
  e.preventDefault();
});

$(function () {
  $('.force-number').keypress(function (e) {
    /* Allow
    0 = CapsLock
    8 = Backspace
    13 = Enter
    45 = '-'
    */
    if (e.which != 0 && e.which != 8 && e.which != 13 && e.which != 45 && (e.which < 48 || e.which > 57)) {
      $('#errmsg-force-number')
        .html('Endast nummer')
        .stop()
        .show()
        .fadeOut('slow');
      return false;
    }
  });
  $('.show-submit-after-12').keyup(function () {
    var len;
    len = this.value.match(/\d/g).length;
    if (len != null && len === 12) {
      $(this)
        .parent()
        .parent()
        .find('button[type="submit"]')
        .removeAttr('disabled');
    }
  });

  $('.all-fields-required-form input').keyup(function () {
    var empty = false;
    var this_form = $(this).closest('.all-fields-required-form');
    // console.log(this_form.get(0));
    $(this_form)
      .find('input')
      .each(function () {
        // console.log($(this).val());
        if ($(this)
          .val() == '') {
          empty = true;
        }
      });
    if (empty) {
      $('.all-fields-required-submit')
        .prop('disabled', true);
    } else {
      $('.all-fields-required-submit')
        .removeAttr('disabled');
    }
  });

  $('.add-hyphen-after-8').keyup(function (event) {
    addHyphen(this);
  });

  function addHyphen(element) {
    var val = $(element)
      .val()
      .split('-')
      .join(''); // Remove dash (-) if mistakenly entered.
    var finalVal = val.match(/.{1,8}/g).join('-'); // Add (-) after 8rd every char.
    $(element)
      .val(finalVal); // Update the input box.
  }
  $('.go-to-person-pass').click(function (e) {
    $('#modal--create-new-person-pass')
      .modal('hide');
    window.location = '../detaljvy/person-pass/index.html';
    e.preventDefault();
  });
  $('.go-to-person-befolkningsregister').click(function (e) {
    $('#modal--create-new-person-befolkningsregister')
      .modal('hide');
    setTimeout(function () {
      window.location = '../detaljvy/person-befolkningsregister/index.html';
    }, 2500);
    $(document).get_external_source_modal();
    e.preventDefault();
  });
});

// modal--create-new-object - STOP

// Tree view, OPTION 1 and 2, toggle attributes-short-view with a touch or click - START
$('#browse-01 ul.tree-view li,#browse-02 ul.tree-view li').on('click', function (e) {
  if (
    $(this)
    .find('ul:first,.has-no-child')
    .is(':visible') &&
    !$(this)
    .is(".item-has-open-search-field")
  ) {
    $(this)
      .removeClass('expanded')
      .find('i:first>svg[data-icon="chevron-down"]')
      .removeClass('fa fa-chevron-down')
      .addClass('fa fa-caret-right');
    $(this)
      .find('ul:first')
      .hide();
    $(this)
      .find('.attributes-short-view')
      .addClass('d-none');
    e.stopPropagation();
  } else if (
    !$(this).is(".item-has-open-search-field")
  ) {
    $(this)
      .find('ul:first')
      .show();
    $(this)
      .addClass('expanded')
      .find('i:first>svg[data-icon="caret-right"]')
      .removeClass('fa fa-caret-right')
      .addClass('fa fa-chevron-down');
    $(this)
      // Unfold selected detail view
      // .find('>.number-of-objects')
      .find('>.click2search-link')
      .after($('.attributes-short-view')
        .removeClass('d-none'));
    $('#collapse-all-branches')
      .parent()
      .removeClass('d-none');
    $('#no-of-objects-in-tree')
      .parent()
      .removeClass('col-xl-6')
      .addClass('col-xl-3');
    e.stopPropagation();
  }
  $('li.has-no-child').on('click', function (e) {
    $(this)
      .find('>.attributes-short-view')
      .toggleClass('d-none');
    e.stopPropagation();
  });
  $('#expand-full-detail-view').on('click', function (e) {
    if (
      !$(this)
      .hasClass('expanded')
    ) {
      $(this)
        .addClass('expanded')
        .html('<i class="fa fa-chevron-down" aria-hidden="true"></i>Visa färre detaljer');
      // Unfold selected detail view
      $('#full-detail-view')
        .removeClass('d-none');
      e.stopPropagation();
      // e.preventDefault();
    } else {
      $('#full-detail-view')
        .addClass('d-none');
      $(this)
        .removeClass('expanded')
        .html('<i class="fa fa-caret-right" aria-hidden="true"></i>Visa fler detaljer');
      e.stopPropagation();
      // e.preventDefault();
    }
    e.preventDefault();
  });
});
// Tree view, OPTION 1 and 2, toggle attributes-short-view with a touch or click - STOP

// Tree view, OPTION 3, unfold attributes-short-view with link - START
$(function () {
  // Add link
  $('#browse-03 ul.tree-view .obj_name')
    .after(
      '<a class="expand-attributes font-italic mx-2" href="#">Visa detaljer</a>'
    );

  $('#browse-03 ul.tree-view li').on('click', function (e) {
    if (
      $(this)
      .find('ul:first,.has-no-child')
      .is(':visible')
    ) {
      $(this)
        .removeClass('expanded')
        .find('i:first>svg[data-icon="chevron-down"]')
        .removeClass('fa fa-chevron-down')
        .addClass('fa fa-caret-right');
      $(this)
        .find('ul:first')
        .hide();
      e.stopPropagation();
    } else {
      $(this)
        .find('ul:first')
        .show();
      $(this)
        .addClass('expanded')
        .find('i:first>svg[data-icon="caret-right"]')
        .removeClass('fa fa-caret-right')
        .addClass('fa fa-chevron-down');
      e.stopPropagation();
    }
  });

  $('#browse-03 ul.tree-view a.expand-attributes').on('click', function (e) {
    if (
      !$(this)
      .hasClass('expanded')
    ) {
      $(this)
        .addClass('expanded')
        .text('Göm detaljer');
      // Unfold selected detail view
      $(this)
        .next('.number-of-objects')
        .after($('.attributes-short-view')
          .removeClass('d-none')
        );
      e.stopPropagation();
      // e.preventDefault();
    } else {
      $(this)
        .next()
        .next('.attributes-short-view')
        .addClass('d-none');
      $(this)
        .removeClass('expanded')
        .text('Visa detaljer');
      e.stopPropagation();
      // e.preventDefault();
    }
    e.preventDefault();
  });
});
// Tree view, OPTION 3, unfold attributes-short-view with link - STOP

// Treeview - STOP

// extend browse view - START
this.addClasses = function (parent, level) {
  // add a class to all <li> elements on this level:
  parent.children('li')
    .addClass('cat-level-' + level);
  // find any child <ul> elements and run this function on them,
  // making sure to increment the level counter:
  if (parent.children('li')
    .children('ul').length) {
    this.addClasses(parent.children('li')
      .children('ul'), level + 1);
  }
};
// start off by selecting only the top-level <ul> elements:
this.addClasses($('ul.dynamic-headings'), 1);

$('ul.tree-view li').filter(function () {
  if ($(this).has('ul').length) {
    $(this)
      .addClass('has-children');
  } else {
    $(this)
      .addClass('has-no-child');
  }
});

// change top-level <li> into heading:
$('li.cat-level-1')
  .contents()
  .filter(function () {
    return this.nodeType === 3 && $.trim(this.nodeValue).length;
  })
  .wrap('<h2 class="obj_name" />');

// hide detail-view for now:
// $('.detail-view').children().addClass('d-none');

// Any list item in the tree represents an attribute name – obj_name
$('li.obj_o,li.obj_l,li.obj_ou,li.obj_cn,#browse-2-section .tree-view>li')
  .contents()
  .filter(function () {
    return this.nodeType === 3 && $.trim(this.nodeValue).length;
  })
  .wrap('<span class="obj_name" />');

var click2search_link = '<a href="#" class="click2search-link float-right font-italic small">Sök härifrån <i class="fa fa-search" aria-hidden="true"></i></a>';
// Add icons
$('.click2search.tree-view.dynamic-icons li.obj_o>.obj_name,.click2search.tree-view.dynamic-icons li.obj_l>.obj_name')
  .before(
    '<i class="fa fa-cubes" aria-hidden="true"></i>'
  )
  .after(click2search_link);

$('.click2search-link').on('mouseover', function () {
  $(this)
    .parent('li')
    .addClass('item-searchable');
  // console.log($(this).parent('li'));
});

$('.click2search-link').on('mouseout', function () {
  $(this)
    .parent('li')
    .removeClass('item-searchable');
  // console.log($(this).parent('li'));
});

$('.click2search-link').on('click', function (e) {

  var click2search_parent = $(this)
    .parent('.item-searchable');

  $(function () {
    $('.item-has-open-search-field')
      .not(click2search_parent)
      .each(function () {
        // $(this).toggleClass("warning");
        $(this)
          .addClass("item-has-closed-search-field")
          .removeClass('item-has-open-search-field');
      });

    $('.item-has-closed-search-field>.click2search-form')
      .replaceWith(click2search_link);

    // Clean up
    $('.item-has-closed-search-field')
      .removeClass('item-has-closed-search-field');

  });

  // console.log($(this).parent('.item-searchable'));

  var click2search_form = '<div class="click2search-form float-right"> \
	<form action="./search-results.html" id="miniSearchForm"> \
		<div class="input-group" role="group" aria-label="sök från trädet"> \
			<input type="search" name="q" placeholder="Namn, titel, HSA-id..." class="form-control"> \
			<div class="input-group-append"> \
				<span class="input-group-text" onclick="document.getElementById(\'miniSearchForm\').submit();">Sök <i class="fa fa-search" aria-hidden="true"></i></span> \
			</div> \
		</div> \
	</form> \
</div> \
';

  $(this)
    .replaceWith(click2search_form);

  // console.log(click2search_parent);

  // $('li').not(click2search_parent).on('click', function (e) {
  //   // console.log($(this));

  //   $('.click2search-form')
  //   .replaceWith(click2search_link);
  // });

  $(click2search_parent)
    .addClass('item-has-open-search-field')
    .removeClass('item-searchable');

  e.stopPropagation();
  e.preventDefault();
});

$('.tree-view.dynamic-icons li.obj_cn>.obj_name')
  .before(
    '<i class="fa fa-user" aria-hidden="true"></i>'
  );

// Wrap each "leaf" in a div
// $('li.has-no-child>.obj_name').wrap('<a href="#" />');
// $('li.has-no-child').wrapInner('<div />');

// Add number-of-objects
$('.tree-view.add-number-of-objects .obj_name')
  .after(
    '<span class="number-of-objects num-gen"></span>'
  );

// Change random number to one for persons
$(function () {
  $('.tree-view.add-number-of-objects .obj_cn>.number-of-objects.num-gen')
    .text('1');
});

// Add short view for all ojects
// $('.dynamic-short-view>li li:first-child').before(
//   $(".attributes-short-view")
// );

// $('.dynamic-short-view .attributes-short-view').wrap('<li />').removeClass("d-none");
// $('body>.attributes-short-view').remove();

// Add quick-view-button
$('.tree-view.add-quick-view-button .obj_name')
  .after(
    '<span class="attributes-quick-view text-nowrap"><i class="fa fa-file-alt" aria-hidden="true"></i>visa</span>'
  );

$('.result-item,.attributes-quick-view').click(function (e) {
  $('#short-view-modal')
    .modal('show');
  e.stopPropagation(); // This prevents branch from expanding
});

$('.go-to-browse').click(function (e) {
  e.preventDefault();
  window.location = 'browse.html';
});

function getNumber() {
  $('.num-gen')
    .each(function () {
      var minNumber = 5;
      var maxNumber = 200;
      var randomnumber = Math.floor(Math.random() * (maxNumber + 1) + minNumber);
      //$(this).html("<b>" + randomnumber + "</b> objekt");
      $(this)
        .html(randomnumber);
    });
}

getNumber();

$('ul.tree-view li:has(> ul)')
  .prepend('<i class="fa fa-caret-right"/>');

// extend browse view - STOP

// Passport or population registrar - START
$(function () {
  $('.has_passport')
    .addClass('d-none')
    .removeClass('d-none');
  $('.has_no_passport')
    .removeClass('d-none')
    .addClass('d-none');
});

$('#passport_popreg_select').click(function () {
  var radioValue = $("input[name='passport_or_popreg']:checked")
    .val();
  // if (radioValue) {
  //   alert("Status - " + radioValue);
  // }
  if (radioValue === 'passport') {
    // var has_passport = true;
    $('.has_passport')
      .addClass('d-none')
      .removeClass('d-none');
    $('.has_no_passport')
      .removeClass('d-none')
      .addClass('d-none');
  } else {
    $('.has_passport')
      .removeClass('d-none')
      .addClass('d-none');
    $('.has_no_passport')
      .addClass('d-none')
      .removeClass('d-none');
  }
});
// Passport or population registrar - STOP

// Add padlock icon to disabled fields - START

$(function () {
  $('.form-control:disabled')
    .after('<i class="fa fa-lock fa-lg disabled-icon" aria-hidden="true"/>')
    .css({
      padding: '.375rem 1.75rem .375rem .75rem'
    });
});

// Add padlock icon to disabled fields - STOP

// Initilize default date time picker - START

// Check if datetimepicker is loaded, then do stuff
if (typeof $.fn.datetimepicker != 'undefined') {
  $(function () {
    $.fn.datetimepicker.Constructor.Default = $.extend({},
      $.fn.datetimepicker.Constructor.Default, {
        icons: {
          time: 'fa fa-clock',
          date: 'fa fa-calendar',
          up: 'fa fa-arrow-up',
          down: 'fa fa-arrow-down',
          previous: 'fa fa-chevron-left',
          next: 'fa fa-chevron-right',
          today: 'fa fa-calendar-check-o',
          clear: 'fa fa-trash',
          close: 'fa fa-times'
        }
      }
    );

    // var d = new Date(); //without params it defaults to "now"
    // var t = new Date().toISOString().substr(0, 19).replace('T', ' ');
    // toISOString() will give you YYYY-MM-DDTHH:mm:ss.sssZ
    var lt = new Date()
      .toISOString()
      .substr(0, 16)
      .replace('T', ' ');
    var l = new Date().toISOString().substr(0, 10);

    // $(".datetimepicker-calendar").datetimepicker({
    //   orientation: 'left top'
    // });

    $('.datetimepicker-calendar>input')
      // .attr('placeholder', lt)
      .val('');

    $(
      "#passportExpireDateCalendar, #hsaVpwInformation2StartCalendar, #hsaVpwInformation2EndCalendar, [id^='startSurgeryHoursCalendar'], [id^='endSurgeryHoursCalendar']"
    ).datetimepicker({
      format: 'L'
    });
    $(
        "#hsaVpwInformation2StartCalendar>input, #hsaVpwInformation2EndCalendar>input, [id^='startSurgeryHoursCalendar']>input, [id^='endSurgeryHoursCalendar']>input"
      )
      // .attr('placeholder', l)
      .val('');
    $('#passportExpireDateCalendar>input')
      // .attr('placeholder', l)
      .val('2024-07-02');
  });
}

// Initilize default date time picker - STOP

// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict';
  window.addEventListener('load', function () {
    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();

// Add and remove form fields dynamically - START

$(function () {
  $('.add-input-group').click(function () {
    var html = $('.copy')
      .html();
    $(this)
      .parent()
      .after(html);
  });
  $('body').on('click', '.remove_parent', function () {
    $(this)
      .parent()
      .parent()
      .remove();
  });
});

// postal adress - START
$('#postalAddressSection2')
  .next('.input-group-append')
  .children('.add-input-group')
  .click(function () {
    $('#postalAddress .form-control')
      .attr('maxlength', '30');
    var InputFields = $('#postalAddressSection2')
      .parent()
      .children('.input-group');
    var numberOfInputFields = $(InputFields).length;
    // console.log(InputFields.last().get(0));
    // console.log(numberOfInputFields);
    if (numberOfInputFields > 1) {
      $(function () {
        // console.log(InputFields.last().children().last().children().parent().parent().children().get());
        // console.log(InputFields.last().children().last().children().parent().parent().children().parent().parent().children().get());
        $(InputFields)
          .last()
          .children()
          .last()
          .children()
          .parent()
          .parent()
          .children()
          .parent()
          .parent()
          .children('.alert')
          .remove(); // Make sure we did not add a disclaimer, if so remove
        $(InputFields)
          .last()
          .after(
            '<div class="alert alert-warning">Adressen får inte vara längre.</div>'
          );
      });
      $(this)
        .parent()
        .children('.add-input-group')
        .addClass('d-none');
    }
    // if (numberOfInputFields < 3) {
    //   $(InputFields).last().children(".disclaimer").remove();
    //   // console.log(InputFields.last().children(".disclaimer").get(0));
    // }

    // $("#postalAddressSection1").parent().children(".input-group").get(0);
    $('#postalAddressSection1')
      .parent()
      .children('.input-group')
      .children('.input-group-append')
      .children('.remove')
      .click(function () {
        $(InputFields)
          .last()
          .children()
          .last()
          .children()
          .parent()
          .parent()
          .children()
          .parent()
          .parent()
          .children('.alert')
          .remove(); // Make sure we did not add a disclaimer, if so remove
        $('#postalAddressSection1')
          .parent()
          .children()
          .children()
          .removeClass('d-none');
      });
  });
// postal address - STOP

// Useless UX function for hsaCommissionRight - START
$(function () {
  $.fn.add_a_button = function () {
    if (selected_a == true && selected_b == true && selected_c == true) {
      selected_from_all_lists = true;
    }
    if (selected_from_all_lists == true && button_added != true) {
      $('#hsaCommissionRight')
        .parent()
        .append(
          '<button class="float-right btn btn-secondary"><i class="fa fa-plus" aria-hidden="true"></i> Lägg till rättighet</button>'
        );
      button_added = true;
    }
  };
  selected_from_all_lists = false;
  button_added = false;
  var selected_a, selected_b, selected_c;
  $('#hsaCommissionRightActivity')
    .parent()
    .click(function () {
      if (selected_from_all_lists == false) selected_a = true;
      $(document).add_a_button();
    });
  $('#hsaCommissionRightInfoType')
    .parent()
    .click(function () {
      if (selected_from_all_lists == false) selected_b = true;
      $(document).add_a_button();
    });
  $('#hsaCommissionRightOrgRange')
    .parent()
    .click(function () {
      if (selected_from_all_lists == false) selected_c = true;
      $(document).add_a_button();
    });
});
// Useless UX function for hsaCommissionRight - STOP

$('#surgeryHoursToggle').click(function () {
  $(this)
    .parent()
    .next('.form-group-toggle')
    .toggleClass('d-none');
  $('#addSurgeryHoursButton,#removeSurgeryHoursButton')
    .toggleClass('d-none');
});

$('#countryNameToggle').click(function () {
  $(this)
    .parent()
    .next('.form-group-toggle')
    .toggleClass('d-none');
});

$("[id^='surgeryHoursAllDay']").click(function () {
  // $("#surgeryHoursAllDay1").click(function () {
  $(this)
    .parent()
    .prev()
    .children()
    .children()
    .children('.surgery-hours-days-container')
    .toggleClass('col-sm-6')
    .toggleClass('col-sm-12');
  $(this)
    .parent()
    .prev()
    .children()
    .children()
    .children('.surgery-hours-times-container')
    .toggleClass('d-none');
  // $(".surgeryHoursAllDay").parent().parent().children().children().children(".surgery-hours-times-container").toggleClass("bg-warning")
});

var weekdays = [
  'Söndag',
  'Måndag',
  'Tisdag',
  'Onsdag',
  'Torsdag',
  'Fredag',
  'Lördag'
];
// console.log(weekdays)

$('.surgeryHoursWeekdays').empty();
$.each(weekdays, function (i, p) {
  $('.surgeryHoursWeekdays')
    .append(
      $('<option></option>')
      .val(p)
      .html(p)
    );
});

var date,
  surgeryHoursMinutesArray = [];
date = new Date();

// Use native JS function padStart that adds a leading zero if a number is less than 10
// console.log(String(5).padStart(2, 0));

// Fix padStart for IE11 - START

// https://github.com/uxitten/polyfill/blob/master/string.polyfill.js
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String/padStart
if (!String.prototype.padStart) {
  String.prototype.padStart = function padStart(targetLength, padString) {
    targetLength = targetLength >> 0; //truncate if number or convert non-number to 0;
    padString = String((typeof padString !== 'undefined' ? padString : ' '));
    if (this.length > targetLength) {
      return String(this);
    } else {
      targetLength = targetLength - this.length;
      if (targetLength > padString.length) {
        padString += padString.repeat(targetLength / padString.length); //append to original to ensure we are longer than needed
      }
      return padString.slice(0, targetLength) + String(this);
    }
  };
}

// Fix padStart for IE11 - STOP

// Here we will find the closest time
while (date.getMinutes() % 15 !== 0) {
  date.setMinutes(date.getMinutes() + 1);
}

// A whole day has 24 * 4 quarters of an hour
// Let's iterate using for loop
for (var i = 0; i < 24 * 4; i++) {
  var h = String(date.getHours()).padStart(2, 0);
  var m = String(date.getMinutes()).padStart(2, 0);
  // console.log(h + ':' + m);
  surgeryHoursMinutesArray.push(h + ':' + m);
  date.setMinutes(date.getMinutes() + 15);
}
// console.log(surgeryHoursMinutesArray);

$('.surgeryHoursMinutes').empty();
$.each(surgeryHoursMinutesArray, function (i, p) {
  $('.surgeryHoursMinutes')
    .append(
      $('<option></option>')
      .val(p)
      .html(p)
    );
});

$('#addSurgeryHoursButton').click(function () {
  var maxInputGroups = 3;
  var thisFormGroup = $(this)
    .parent()
    .parent()
    .children('.form-group-toggle');
  // $(thisFormGroup).toggleClass("bg-warning");
  // $("#addSurgeryHoursButton").parent().parent().children('.form-group-toggle').toggleClass("bg-warning");
  // $("#addSurgeryHoursButton").parent().parent().children('.form-group-toggle').children('.surgery-hours-group').toggleClass("bg-warning");

  if (
    $(thisFormGroup)
    .children('.surgery-hours-group').length + 1 >
    maxInputGroups
  ) {
    $(thisFormGroup).wrapInto(
      '<div class="alert alert-warning"><a href="#" class="close" data-dismiss="alert">&times;</a>Endast ' +
      maxInputGroups +
      ' poster tillåtna</div>'
    );
    return false;
  }
  var id = (
    $(thisFormGroup)
    .children('.surgery-hours-group').length + 1
  ).toString();
  // console.log(id);

  $(thisFormGroup)
    .append(
      '<div class="surgery-hours-group input-group pb-3 my-3 border-bottom" id="surgeryHoursGroup' +
      id +
      '"><div class="form-row no-gutters container"><div class="form-group col-sm-6 border pb-1"><div class="form-row"><label class="col-sm-12">Från</label><div class="col-sm-6 surgery-hours-days-container"><select class="surgeryHoursWeekdays"><option>Veckodag</option></select></div><div class="col-sm-6 surgery-hours-times-container"><select class="surgeryHoursMinutes"><option>Timme:minut</option></select></div></div></div><div class="form-group col-sm-6 border"><div class="form-row"><label class="col-sm-12">Till</label><div class="col-sm-6 surgery-hours-days-container"><select class="surgeryHoursWeekdays"><option>Veckodag</option></select></div><div class="col-sm-6 surgery-hours-times-container"><select class="surgeryHoursMinutes"><option>Timme:minut</option></select></div></div></div></div><div class="form-check container pb-2"><input class="form-check-input surgeryHoursAllDay" type="checkbox" name="surgeryHoursAllDay' +
      id +
      '" id="surgeryHoursAllDay' +
      id +
      '" value="surgeryHoursActive"><label class="form-check-label" for="surgeryHoursAllDay' +
      id +
      '">Öppet hela dygnet</label></div><div class="form-group container no-gutters"><label for="surgeryHoursComment' +
      id +
      '">Kommentar</label><textarea class="form-control" id="surgeryHoursComment' +
      id +
      '" rows="3"></textarea></div><div class="form-row container no-gutters"><div class="input-group date datetimepicker-calendar col-sm-6" id="startSurgeryHoursCalendar' +
      id +
      '" data-target-input="nearest"><label for="startSurgeryHours' +
      id +
      '">Gäller fr.o.m.</label><input type="text" id="startSurgeryHours' +
      id +
      '" class="form-control" data-toggle="datetimepicker" data-target="#startSurgeryHoursCalendar' +
      id +
      '"><div class="input-group-append" data-target="#startSurgeryHoursCalendar' +
      id +
      '" data-toggle="datetimepicker"><div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div></div></div><div class="input-group date datetimepicker-calendar col-sm-6" id="endSurgeryHoursCalendar' +
      id +
      '" data-target-input="nearest"><label for="endSurgeryHours' +
      id +
      '">Gäller t.o.m.</label><input type="text" id="endSurgeryHours' +
      id +
      '" class="form-control" data-toggle="datetimepicker" data-target="#endSurgeryHoursCalendar' +
      id +
      '"><div class="input-group-append" data-target="#endSurgeryHoursCalendar' +
      id +
      '" data-toggle="datetimepicker"><div class="input-group-text"><i class="fa fa-calendar" aria-hidden="true"></i></div></div></div></div></div>'
    );

  $('.surgeryHoursWeekdays').empty();
  $.each(weekdays, function (i, p) {
    $('.surgeryHoursWeekdays')
      .append(
        $('<option></option>')
        .val(p)
        .html(p)
      );
  });

  $('.surgeryHoursMinutes').empty();
  $.each(surgeryHoursMinutesArray, function (i, p) {
    $('.surgeryHoursMinutes')
      .append(
        $('<option></option>')
        .val(p)
        .html(p)
      );
  });

  $('#surgeryHoursAllDay' + id).click(function () {
    $(this)
      .parent()
      .prev()
      .children()
      .children()
      .children('.surgery-hours-days-container')
      .toggleClass('col-sm-6')
      .toggleClass('col-sm-12');
    $(this)
      .parent()
      .prev()
      .children()
      .children()
      .children('.surgery-hours-times-container')
      .toggleClass('d-none');
  });

  $('select')
    .each(function () {
      $(this).select2({
        language: 'sv',
        // theme: 'bootstrap4',
        width: '100%',
        placeholder: $(this)
          .attr('data-placeholder'),
        allowClear: Boolean($(this).data('allow-clear'))
      });
    });
  // Allow user input that is not in the list
  $('.surgeryHoursMinutes').select2({
    tags: true
  });

  var lt = new Date()
    .toISOString()
    .substr(0, 16)
    .replace('T', ' ');
  var l = new Date().toISOString().substr(0, 10);

  $(
    "[id^='startSurgeryHoursCalendar'], [id^='endSurgeryHoursCalendar']"
  ).datetimepicker({
    format: 'L'
  });
  $(
      "[id^='startSurgeryHoursCalendar']>input, [id^='endSurgeryHoursCalendar']>input"
    )
    .attr('placeholder', l)
    .val('');
});

$('#removeSurgeryHoursButton').click(function () {
  var thisFormGroup = $(this)
    .parent()
    .parent()
    .children('.form-group-toggle');
  if ($(thisFormGroup)
    .children().length == 1) {
    $(thisFormGroup)
      .append('Det finns inga fler poster att ta bort');
    return false;
  }
  $(thisFormGroup)
    .children('.surgery-hours-group')
    .last()
    .remove();
});

// Add and remove form fields dynamically - STOP

// Confidential funtionallity - START
$(function () {
  // We need to be able to click and view disabled input fields – let's unlock
  $('.confidential')
    .attr('disabled', false)
    .addClass('temporary-enabled'); // Element(s) are now enabled.
  $('.confidential')
    .each(function (index) {
      var confidentialField = $(this);
      // console.log('Type: ' + input.attr('type') + '\nName: ' + input.attr('name') + '\nValue: ' + input.val());
      //confidentialVal is your string of characters (with asterisks).
      confidentialVal = confidentialField.val();
      var obfuscatedVal = confidentialVal.replace(/[\S]/g, '*');
      confidentialField.val(obfuscatedVal);
      $(this)
        .attr('data-top-secret', confidentialVal);
      $(this).after(
        '<i class="fa fa-eye-slash fa-lg confidential-icon closed-eye" aria-hidden="true"/>'
      );
    });

  $('.confidential-link')
    .next()
    .toggleClass('confidential-link-icon');

  $('.confidential').click(function () {
    var el = $(this);
    var confidentialValRecreated = $(this)
      .attr('data-top-secret');
    var obfuscatedValRecreated = $(this)
      .val()
      .replace(/[\S]/g, '*');
    // var theClass = $(this).attr('class');
    // console.log(theClass);
    // console.log($(this).get(0));
    // console.log($(this).next().get(0));
    // $(this).next('.confidential-icon').toggle();
    // console.log('Top secret!');
    $(".generic-confirmation-modal")
      .remove(); // Remove the standard modal and create a new one
    confirmDialog(
      "<p><b>Denna information är konfidentiell. Om du väljer att visa informationen kommer detta att loggas.</b></p><label for='reasonToShow' style='text-align:left'>Ange orsak till att visa informationen (valfritt)</label><textarea class='form-control' id='reasonToShow' rows='2'></textarea>", "Visa konfidentiell information", "Avbryt"
    );
    modalConfirm(function (confirm) {
      if (confirm) {
        // console.log("yes, yes, yes");
        // console.log(el.get(0));
        el
          .addClass('edit-with-permission')
          .removeAttr('data-top-secret')
          .off("click")
          .next('.confidential-icon.closed-eye')
          .replaceWith(
            '<i class="fa fa-eye fa-lg confidential-icon open-eye" aria-hidden="true"/>'
          );
        el.removeClass('temporary-enabled');
        // el.parent().after('<div class="mock-message mock-message-note px-2">Pennan visar att det är möjligt att ändra. Här ska en dialogruta visas om man klickar. Det är samma varning som att redigera legitimerad yrkesgrupp.</div>')

        // Special cases...
        // Special case 1 - START
        $('#personalPrescriptionCode')
          .next()
          .next()
          .after('<div class="mock-message mock-message-note px-2">Pennan visar att det är möjligt att ändra. Här ska en dialogruta visas om man klickar. Det är samma varning som att redigera legitimerad yrkesgrupp.</div>');
        $('#personalPrescriptionCode')
          .addClass('temporary-enabled')
          .after('<i class="fa fa-pen fa-lg edit-icon editing-enabled" aria-hidden="true"/>');
        // el.parent().children(".edit-icon.editing-disabled").remove(); // Remove pen icon
        // el.parent().children(".disabled-icon").remove(); // Remove padlock icon
        // var personalPrescriptionCodeDialog = el.parent().children(".edit-icon.editing-disabled"); // Grab pen icon
        // $('#personalPrescriptionCode').after(personalPrescriptionCodeDialog); // Move pen icon
        // Special case 1 - STOP

        // Special case 2 - START
        $('#personalIdentityNumber')
          .addClass('temporary-enabled');
        // Special case 2 - STOP

        // console.log(el).attr('data-top-secret'));
        // console.log(el.val());
        el.val(confidentialValRecreated);
        // el.removeClass('temporary-enabled');
        // In case this is SITHS-information
        $('#more-detailed,#less-detailed')
          .toggleClass('d-none');
        $('#less-detailed .confidential-link')
          .next()
          .toggleClass('confidential-link-icon');
      } else {
        // console.log("no, no, no");
        el.addClass('temporary-enabled');
      }
    });
    if (
      $(this)
      .next('.confidential-icon')
      .hasClass('closed-eye')
    ) {
      // Nothing to do
    } else {
      $(this)
        .next('.confidential-icon.open-eye')
        .replaceWith(
          '<i class="fa fa-eye-slash fa-lg confidential-icon closed-eye" aria-hidden="true"/>'
        );
      $(this)
        .val(obfuscatedValRecreated);
    }
    $(".generic-confirmation-modal")
      .modal('show');
  });

});
// Confidential funtionallity - STOP

// Edit-with-permission funtionallity - START
// What icons do we have insatalled...
// $('body').append('<i class="fa fa-pen"/>');

$(function () {
  // We need to be able to click and view disabled input fields – let's unlock
  $('.edit-with-permission')
    .attr('disabled', false)
    .addClass('temporary-enabled'); // Element(s) are now enabled.
  $('.edit-with-permission')
    .each(function (index) {
      // console.log('Type: ' + input.attr('type') + '\nName: ' + input.attr('name') + '\nValue: ' + input.val());
      $(this).after(
        '<i class="fa fa-pen fa-lg edit-icon editing-disabled" aria-hidden="true"/>'
      );
    });
  $('.edit-with-permission').click(function () {
    if (
      $(this)
      .next('.edit-icon')
      .hasClass('editing-disabled')
    ) {

      $(".generic-confirmation-modal")
        .remove(); // Remove the standard modal and create a new one
      confirmDialog(
        '<p><b>Informationen kan tillfälligt hanteras manuellt om uppgiften inte kan inhämtas automatiskt. Har du kontrollerat (per telefon eller e-post) med Socialstyrelsen att personen har denna legitimerade yrkesgrupp/förskrivarkod?</b></p>', "Ja, hantera manuellt", "Avbryt");

      modalConfirm(function (confirm) {
        if (confirm) {
          // console.log("Yes!");
          $('.disabled-icon')
            .remove();
          $(this)
            .removeClass('temporary-enabled')
            .focus();
          $(this)
            .next('.edit-icon.editing-disabled')
            .replaceWith(
              '<i class="fa fa-pen-square fa-lg edit-icon editing-enabled" aria-hidden="true"/>'
            );
        } else {
          // Happens if answer is no
          // console.log("No!");
        }
      });
      $(".generic-confirmation-modal")
        .modal('show');
    } else {
      console.log("editing-enabled already set");
    }
  });
});

// Edit-with-permission funtionallity - STOP

// Get data from external source (fake) - START

// Get external source message - START
$(function () {
  $.fn.get_external_source_modal = function () {
    $('body')
      .append(
        '<div class="modal fade" id="spinner-message-1" role="dialog"><div class="modal-dialog modal-dialog-centered"><!--Modal content--><div class="modal-content"><div class="modal-body"><a href="#" class="close float-right" data-dismiss="modal">&times;</a><h4 class="text-center">Hämtar extern data. Det tar en liten stund...</h4><div class="text-center"><span class="spinner-grow text-secondary" style="width: 3rem; height: 3rem;" role="status"></span></div></div></div></div></div>'
      );

    //Trigger the modal
    $('#spinner-message-1')
      .modal({
        backdrop: 'static',
        keyboard: false
      });

    //Remove the modal once it is closed.
    $('#spinner-message-1').on('hidden.bs.modal', function () {
      $('#spinner-message-1').fade();
    });

    //Or move after 5 seconds if not closed
    setTimeout(function () {
      $('#spinner-message-1')
        .remove();
      $('.modal-backdrop')
        .remove();
      $('body')
        .removeClass('modal-open');
      $('#get-hosp-info, #get-popreg-info')
        .attr('aria-pressed', 'false')
        .removeClass('active');
    }, 4000);
  };
});
// Get external source message - STOP

$('#get-hosp-info, #get-popreg-info').click(function () {
  $(document).get_external_source_modal();
});

// Get data from external source (fake) - STOP

// select2.org - START

$(function () {
  $('select')
    .each(function () {
      $(this).select2({
        templateResult: function (data) {
          // Make options stylable (indent levels with .l2, .l3 etc.).  We only really care if there is an element to pull classes from
          if (!data.element) {
            return data.text;
          }
          var $element = $(data.element);
          var $wrapper = $('<span></span>');
          $wrapper.addClass($element[0].className);
          $wrapper.text(data.text);
          return $wrapper;
          //Options are now stylable
        },
        language: 'sv',
        // theme: 'bootstrap4',
        width: '100%',
        placeholder: $(this)
          .attr('data-placeholder'),
        allowClear: Boolean($(this).data('allow-clear'))
      });
    });
  // Omitted:
  $('#no-of-rows').select2('destroy');
  // Allow user input that is not in the list
  $('.surgeryHoursMinutes').select2({
    tags: true
  });
});

// Filtrera sökområde = searchable_locactions - START

var searchable_locactions = [];
$.getJSON(
  '/' + firstLevelLocation + '/assets/search-area-medium-190209.json',
  function (data) {
    //parse json
    searchable_locactions = data;
    // console.log(searchable_locactions);
  }
);

$('#search-within-org').select2({
  data: {
    results: searchable_locactions,
    text: 'name'
  },
  placeholder: $(this)
    .attr('data-placeholder'),
  allowClear: Boolean($(this).data('allow-clear'))
});

$(document).ajaxStop(function () {
  var options = $('#search-within-org');
  $.each(searchable_locactions, function () {
    options.append($('<option />')
      .text(this.name));
  });
});

// Filtrera sökområde = searchable_locactions - STOP

$('#search-within-org').on('select2:selecting', function (e) {
  // what you would like to happen when making a selection
  set_selected_org = function () {
    var selected_org = $('.select2-selection__rendered')
      .html();
    $('.filter-search-within-org')
      .addClass('filter-changed');
    $('.filter-search-within-org')
      .html(selected_org);
  };
  setTimeout(set_selected_org, 100);
  setTimeout(function () {
    $('.filter-search-within-org')
      .toggleClass('alert_here');
  }, 500);
  setTimeout(function () {
    $('.filter-search-within-org')
      .toggleClass('alert_here');
  }, 1000);
});

// select2.org - STOP

// Step Wizard - START

$(function () {
  var navListItems = $('div.setup-panel div a'),
    allWells = $('.setup-content'),
    allNextBtn = $('.nextBtn'),
    allPrevBtn = $('.prevBtn');

  allWells.hide();

  navListItems.click(function (e) {
    e.preventDefault();
    var $target = $($(this)
        .attr('href')),
      $item = $(this);

    if (!$item.hasClass('disabled')) {
      navListItems.removeClass('active')
        .addClass('wizard-step');
      $item.addClass('active');
      allWells.hide();
      $target.show();
      $target.find('input:eq(0)').focus();
    }
  });

  allPrevBtn.click(function () {
    var curStep = $(this)
      .closest(".setup-content"),
      curStepBtn = curStep.attr("id"),
      prevStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]')
      .parent()
      .prev()
      .children("a");
    prevStepWizard.removeAttr('disabled')
      .trigger('click');
  });

  allNextBtn.click(function () {
    var curStep = $(this)
      .closest(".setup-content"),
      curStepBtn = curStep.attr("id"),
      nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]')
      .parent()
      .next()
      .children("a"),
      curInputs = curStep.find("input[type='text'],input[type='url']"),
      isValid = true;

    $(".form-group")
      .removeClass("has-error");
    for (var i = 0; i < curInputs.length; i++) {
      if (!curInputs[i].validity.valid) {
        isValid = false;
        $(curInputs[i])
          .closest(".form-group")
          .addClass("has-error");
      }
    }

    if (isValid)
      nextStepWizard.removeAttr('disabled')
      .trigger('click');
  });

  $('div.setup-panel div a.active')
    .trigger('click');

  // Conditional inputs - START

  $('#wiz-1-opt-col-1 input').click(function () {
    var opt_attr_value = $("input[name='opt_attr']:checked")
      .val();
    // if (opt_attr_value) {
    //   alert("Status - " + opt_attr_value);
    // }
    if (opt_attr_value === 'true') {
      $('#wiz-1-opt-col-2,#wiz-1-opt-col-3')
        .addClass('d-none')
        .removeClass('d-none');
      $('#wiz-1-opt-container-1')
        .removeClass('offset-lg-4');
    } else {
      $('#wiz-1-opt-col-2,#wiz-1-opt-col-3')
        .removeClass('d-none')
        .addClass('d-none');
      $('#wiz-1-opt-container-1')
        .addClass('offset-lg-4');
    }
  });
  $('#wiz-1-opt-exists-3-string').click(function () {
    $("#wiz-1-opt-exists-3")
      .prop("checked", true);
  });

  // Conditional inputs - STOP
});

// Step Wizard - STOP

// Dynamic tooltips from placeholders

$(function () {
  $(':input').mouseenter(function () {
    // $(this).attr("placeholder", "New Value");
    var attr = $(this)
      .attr('placeholder');
    // For some browsers, `attr` is undefined; for others, `attr` is false. Check for both.
    if (typeof attr !== typeof undefined && attr !== false) {
      // Element has this attribute
      var val = $(this)
        .attr('placeholder');
      $(this)
        .attr('title', val);
    }
  });
});

// Enable tooltips everywhare
$('[data-toggle="tooltip"]').tooltip();

// Stop default link click behavior for nav-links
$('.mock-link, .sidebar-left-toggle>.toggle-sidebar, .obj_name')
  .click(function (e) {
    e.stopPropagation();
    e.preventDefault();
  });

// Detect if topnav is outside viewport - START

$(document).scroll(function () {
  if ($(document).scrollTop() >= 80) {
    // user scrolled 80 pixels or more;
    // do stuff
    $('body')
      .addClass('topnav-outside-viewport');
    $('.topnav-outside-viewport .toggle-sidebar').on('click', function () {
      window.scrollTo(0, 0);
      // $("html, body").animate({
      //   scrollTop: 0
      // }, "slow");
    });
  } else {
    $('body')
      .removeClass('topnav-outside-viewport');
  }
});

// Detect if topnav is outside viewport - STOP

// Cookie message - START

if (Cookies.get("acceptCookies") && Cookies.get("acceptCookies") === "true") {
  // have cookie
  // alert('I collect cookies!');
  // $('body').removeClass('has-cookie-alert');
} else {
  // no cookie
  // alert('I am concidering accepting cookies but still not sure.');
  $('body')
    .addClass('has-cookie-alert');
}

$('#cookie-alert-wrapper .acceptcookies').click(function () {
  $('section[role="main"]')
    .css({
      'min-height': 'calc(100vh - 17rem)',
      transition: 'min-height 1s'
    });
});

// Only prototype setting - START

$(document).ajaxStop(function () {

  var isAdminSetting = localStorage.getItem("isAdmin");
  if (isAdminSetting !== null) {
    $("input[name='checkboxAminRights']")
      .attr("checked", "checked");
    $('body')
      .addClass('is-admin');
    console.log('User has admin privilegies');
  } else {
    $("#toggle-prototype-settings")
      .text('Per Persson');
    $('body')
      .removeClass('is-admin');
    console.log('User has no privilegies');
    $('.only-for-admin,#top-nav-admin,#left-nav-item-02,#left-nav-item-03,#left-nav-item-04,#edit-mode-menu,.short-view-edit')
      .addClass('d-none');
    $('#personalId,#passportId')
      .parent()
      .addClass('d-none');

    $('a')
      .each(function () {
        var value = $(this)
          .attr('href');
        $(this)
          .attr('href', value.replace(/detaljvy\/(.*?)\/index\.html/g, 'detaljvy\/$1\/view\.html'));
      });

  }

  $("input[name='checkboxAminRights']").click(function () {

    if ($(this).is(":checked")) {
      localStorage
        .setItem("isAdmin", 1);
    } else {
      localStorage
        .removeItem("isAdmin");
      // localStorage.clear()
    }
    location.reload();
  });

  var showReqSetting = localStorage.getItem("hasRequirements");
  if (showReqSetting !== null) {
    $("input[name='checkboxReqSettings']")
      .attr("checked", "checked");
    console
      .log('Requirements shown');
  } else {
    console
      .log('Requirements hidden');
  }

  $("input[name='checkboxReqSettings']").click(function () {
    if ($(this).is(":checked")) {
      localStorage
        .setItem("hasRequirements", 1);
    } else {
      localStorage
        .removeItem("hasRequirements");
      // localStorage.clear()
    }
    location.reload();
  });

  // show-cookie-disclaimer-toggle
  $('.has-cookie-alert #show-cookie-disclaimer-toggle')
    .addClass('d-none');
  $('.show-cookie-disclaimer').click(function (e) {
    $('body')
      .addClass('has-cookie-alert');
    $('.cookiealert')
      .modal('show');
    $('.modal-backdrop')
      .removeClass("modal-backdrop");
    Cookies.remove("acceptCookies");
    e.stopPropagation();
    e.preventDefault();
  });

});

// Only prototype setting - STOP

// Cookie message - STOP

// WCAG - START

$(function () {
  $('body').prepend(
    '<a href="#main-content-wrapper" class="sr-only sr-only-focusable"><i class="fa fa-arrow-down" aria-hidden="true"></i>Hoppa till huvudinnehåll</a>'
  );
});

// WCAG - STOP

// Media queries - START

/*
xs = Extra small <576px
sm = Small ≥576px
md = Medium ≥768px
lg = Large ≥992px
xl = Extra large ≥1200px
*/

$.fn.removeClassRegex = function (regex) {
  return $(this)
    .removeClass(function (index, classes) {
      return classes.split(/\s+/).filter(function (c) {
        return regex.test(c);
      }).join(' ');
    });
};

$(window).on("load resize", function () {
  var pageWidth = $(window).width();
  if ($(window).width() < 576) {
    $('body')
      .removeClassRegex(/-screen/);
    $('body')
      .addClass('xs-screen');
    //alert("less than @media screen and (min-width: 576px)");
  }
  if ($(window).width() >= 576) {
    $('body')
      .removeClassRegex(/-screen/);
    $('body')
      .addClass('sm-screen');
  }
  if ($(window).width() >= 768) {
    $('body')
      .removeClassRegex(/-screen/);
    $('body')
      .addClass('md-screen');
  }
  if ($(window).width() >= 992) {
    $('body')
      .removeClassRegex(/-screen/);
    $('body')
      .addClass('lg-screen');
  }
  if ($(window).width() >= 1200) {
    $('body')
      .removeClassRegex(/-screen/);
    $('body')
      .addClass('xl-screen');
  }
});

setTimeout(function () {
  var sidebarLeftCopy;
  $("#sidebar-left")
    .html(sidebarLeftCopy);

  $(".sm-screen #sidebar-left, .xs-screen #sidebar-left")
    .addClass('ml-0')
    .appendTo('#top-nav-wrapper');
  $(".md-screen #sidebar-left, .lg-screen #sidebar-left, .xl-screen #sidebar-left")
    .removeClass('ml-0');
  $(".md-screen #sidebar-left-placeholder, .lg-screen #sidebar-left-placeholder, .xl-screen #sidebar-left-placeholder")
    .after(sidebarLeftCopy);
  $(".sm-screen #sidebar-left, .xs-screen #sidebar-left")
    .css({
      'min-width': 'calc(100vw - 5em)',
      'box-shadow': 'none'
    });
  $(".sm-screen .toggle-sidebar, .xs-screen .toggle-sidebar")
    .css({
      'display': 'none'
    });
}, 1000);

// Media queries - STOP
