/* jshint multistr: true */

// Tree view - START
$('ul.tree-view li').on('click', function (e) {
  if (
    $(this)
    .find('ul:first,.has-no-child')
    .is(':visible')
  ) {
    $(this)
      .removeClass('expanded')
      .find('i:first>svg[data-icon="chevron-down"]')
      .removeClass('fa fa-chevron-down')
      .addClass('fa fa-caret-right');
    $(this)
      .find('ul:first')
      .hide();
    e.stopPropagation();
  } else if (
    $(this).is(".has-children")
  ) {
    $(this)
      .find('ul:first')
      .show();
    $(this)
      .addClass('expanded')
      .find('i:first>svg[data-icon="caret-right"]')
      .removeClass('fa fa-caret-right')
      .addClass('fa fa-chevron-down');
    $(this)
      // Unfold selected detail view
        .removeClass('d-none');
    $('#collapse-all-branches')
      .parent()
      .removeClass('d-none');
    $('#no-of-objects-in-tree')
      .parent()
      .removeClass('col-xl-6')
      .addClass('col-xl-3');
    e.stopPropagation();
  }
  $('#expand-full-detail-view').on('click', function (e) {
    if (
      !$(this)
      .hasClass('expanded')
    ) {
      $(this)
        .addClass('expanded');
      e.stopPropagation();
      // e.preventDefault();
    }
    e.preventDefault();
  });
});
// Tree view - STOP

// document.querySelector("#browse-1-section > ul > li.obj_o.cat-level-1.has-children.expanded > ul > li.obj_o.cat-level-2.has-children.expanded > ul > li.obj_cn.cat-level-3.has-no-child");

$(function () {
  // Expand filter menu by default
  // Make sure we do not duplicate classes

  $('#sidebar-left, .sidebar-border')
    .removeClass('active')
    .addClass('active');
  $('#main-content-wrapper')
    .removeClass('col-sm-7')
    .addClass('col-sm-7')
    .removeClass('offset-sm-4')
    .addClass('offset-sm-4')
    .removeClass('col-sm-12')
    .addClass('col-sm-12');
  $('.sidebar-left-toggle>.toggle-sidebar')
    .toggleClass('opened closed');
  $('#filter-submenu')
    .removeClass('show')
    .addClass('show');
  $('#left-nav-item-01>.dropdown-toggle')
    .attr('aria-expanded', 'true');

  // Reveal person to be moved - START
  // 1st level, 3rd item, arrow down
  $('#browse-1-section>ul.tree-view>li:nth-child(3)')
    .find('i:first>svg[data-icon="caret-right"]')
    .removeClass('fa fa-caret-right')
    .addClass('fa fa-chevron-down');
  // 1st level, 3rd item, unfold
  $('#browse-1-section>ul.tree-view>li:nth-child(3)')
    .addClass('expanded')
    .find('ul:first')
    .show();
  // 2nd level, 2nd item, arrow down
  $('#browse-1-section>ul.tree-view>li.obj_o.cat-level-1.has-children.expanded>ul>li.obj_o.cat-level-2.has-children')
    .find('i:first>svg[data-icon="caret-right"]')
    .removeClass('fa fa-caret-right')
    .addClass('fa fa-chevron-down');
  $('#browse-1-section>ul.tree-view>li.obj_o.cat-level-1.has-children.expanded>ul>li.obj_o.cat-level-2.has-children')
    .addClass('expanded')
    .find('ul:first')
    .show();
  $('#browse-1-section>ul.tree-view .number-of-objects')
    .empty()
    .addClass('btn btn-sm')
    .removeClass('num-gen')
    .prepend('<a href="#" class="px-1 text-secondary text-light move-to-here">flytta hit</a>');
  // Reveal person to be moved - STOP

  object_to_be_moved = $('#browse-1-section>ul.tree-view>li.obj_o.cat-level-1.has-children.expanded>ul>li.obj_o.cat-level-2.has-children.expanded>ul>li.obj_cn.cat-level-3.has-no-child');
  $(object_to_be_moved)
    .addClass('bg-warning')
    // Replace number
    .find('.number-of-objects')
    .addClass('font-italic')
    .text('objektet kan nu flyttas');

  // console.log($(object_to_be_moved).get(0));
  // console.log($(object_to_be_moved).find('.number-of-objects').get(0));

  $('.move-to-here').click(function (e) {
    $(object_to_be_moved)
      .appendTo(
        $(this)
        .parent()
        .parent()
      )
      .find('.number-of-objects.btn.btn-sm.font-italic')
      .addClass('bg-default')
      .html('objektet har flyttats – <a href="#" id="accept-move">bekräfta</a> eller <a href="#" id="reject-move">ångra</a>');
    e.preventDefault();
    $(function () {
      $('#accept-move').click(function (e) {
        $(this)
          .parent()
          .html('objektet har flyttats');
        e.preventDefault();
      });
      $('#reject-move').click(function (e) {
        location.reload();
        e.preventDefault();
      });

    });
  });

});
