/* jshint multistr: true */

$('.remove-personal-data').on('click', function (e) {
  var mock_message_remove_personal_data_modal = '\
      <div id="mock-message-remove-personal-data-modal" class="modal fade mock-message-note" tabindex="-1" role="dialog" aria-hidden="true">\
        <div class="modal-dialog modal-max-700px modal-dialog-centered">\
        <!-- Modal content-->\
        <div class="modal-content">\
          <div class="modal-body text-left">\
            <a href="#" class="close" data-dismiss="modal">&times;</a>\
            <div class="pb-3">\
            <h3 class="text-center">Vad händer här i det riktiga gränssnittet?</h3>\
            <p>När man söker en person som man vill rensa helt ur katalogen kan tre saker hända (och som leder till totalt fyra vägar för flödet):</p>\
             <ol type="1">\
              <li>\
                <p>Personen finns inte alls i HSA (inte ens i Limbo)</p>\
                <ol type="a">\
                  <li>\
                    <p>Om så är fallet ska användaren få ett meddelande om detta och flödet avslutas</p>\
                  </li>\
                </ol>\
              </li>\
              <li>\
                <p>Personen finns i Limbo</p>\
                <ol type="a">\
                  <li>\
                    <p>Om så är fallet ska användaren få ett meddelande om att personen finns i Limbo och senast är ändrad vid det datum som är angivet i attributet "senast ändrad".</p>\
                  </li>\
                  <li>\
                    <p>Därefter ska användaren tillfrågas om hen vill gå vidare och kontrollera om personen kan raderas eller avbryta.</p>\
                  </li>\
                  <li>\
                    <p>Om användaren väljer att gå vidare ska en kontroll mot historiken i databasen simuleras (i prototypen, alltså). Därefter ska användaren få besked om antingen:</p>\
                    <ol>\
                      <li>\
                        <p>Att personen kan raderas, och därefter få bekräfta att radering ska göras (eller avbrytas)</p>\
                      </li>\
                      <li>\
                        <p>Att personen inte kan raderas, eftersom vederbörande (1) har varit kopplad till vårdmedarbetaruppdraget "Vård och behandling Äldreboendet Blomman" till och med 2019-11-19 och därmed inte kan raderas förrän tidigast 2024-11-20 och (2) har varit utpekad som HSA-administratör för enheten "Äldreboendet Blomman" till och med 2019-11-19 och därmed inte kan raderas förrän tidigast 2024-11-20 . Här avslutas flödet.</p>\
                      </li>\
                    </ol>\
                  </li>\
                </ol>\
              </li>\
              <li>\
                <p>Personen finns aktiv i HSA</p>\
                <ol type="a">\
                  <li>\
                    <p>Om så är fallet ska användaren få ett meddelande om detta och uppmanas att först ta bort personen om det är möjligt enligt organisationens riktlinjer och därefter åter välja att hantera begäran om radering av personuppgifter. Här avslutas flödet.</p>\
                  </li>\
                </ol>\
              </li>\
            </ol>\
            <p>OBS! Det kan också förekomma att personen finns som aktiv eller i Limbo hos en annan organisation. Då måste administratören informeras om detta och uppmanas att vidareförmedla denna information till den berörda personen. Detta är dock inte implementerat i prototypen.</p>\
            </div>\
          <div class="d-flex justify-content-between no-gutters">\
          </div>\
        </div>\
      </div>\
    </div>\
  </div>';
  $(mock_message_remove_personal_data_modal).appendTo('body');
  $("#mock-message-remove-personal-data-modal")
    .modal('show');
});

// Scenarios based on search
$(function () {
  $('#radering-av-personuppgifter form').on('submit', function (e) { //use on if jQuery 1.7+
    // e.preventDefault(); //prevent form from submitting
    if ($("#radering-av-personuppgifter form :input")
      .val().length && $("#radering-av-personuppgifter form :input")
      .val() === '000') {
      $(this)
        .attr('action', function (i, v) {
          return v.replace(/user-in-limbo\.html/g, 'user-nowhere-to-be-found.html');
        });
    } else if ($("#radering-av-personuppgifter form :input")
      .val().length && $("#radering-av-personuppgifter form :input")
      .val() === '111') {
      $(this)
        .attr('action', function (i, v) {
          return v.replace(/user-in-limbo\.html/g, 'user-exists-in-index.html');
        });
    }
  });
  var search_term_in_result = getUrlParameter('q');
  $('.search-term-in-result')
    .text(search_term_in_result);
});

// $(function () { //shorthand document.ready function
//   $('#radering-av-personuppgifter form').on('submit', function (e) { //use on if jQuery 1.7+
//     e.preventDefault(); //prevent form from submitting
//     var data = $("#radering-av-personuppgifter form :input").serializeArray();
//     console.log(data); //use the console for debugging, F12 in Chrome, not alerts
//   });
// });
