/* jshint multistr: true */

// console.log('additional script loaded');

// Select care provider and care unit

// Detail view - check isCareUnit/hsaHealthCareUnit - START

$(function () {
  $('#hsaHealthCareProvider').click(function () {
    isCareProviderValue = $("input[name='hsaHealthCareProvider']").prop(
      'checked'
    );
    // console.log('isCareProviderValue is ' + isCareProviderValue);
    if (isCareProviderValue == false) {
      $(this)
        .parent()
        .after(
          '<div class="alert alert-warning" id="hsaHealthCareProviderDisclaimer001" role="alert">\
          <p><i>Blekingesjukhuset Karlskrona</i> kan inte avmarkeras som vårdgivare.</p>\
          <p>Enheten är angiven som ansvarig vårdgivare för följande vårdenheter, hantera dessa vårdenheter först</p>\
          <p>Den (eller dessa) vårdenhet(er) behöver hanteras först:</p>\
          <ul>\
            <li><a href="#" class="mock-link mock-message">Enhet 1</a></li>\
            <li><a href="#" class="mock-link mock-message">Enhet 2</a></li>\
            <li><a href="#" class="mock-link mock-message">Enhet 3</a></li>\
          </ul>\
          <p>[Prototypmeddelande: om listan är längre än 10 visas de 10 översta med en skrollningslist som gör det möjligt att visa fler]</p >\
          </div>\
        '
        );
        $("input[name='hsaHealthCareProvider']").prop("checked", true);
      $('#hsaHealthCareProviderDisclaimer001').click(function () {
        $(this)
          .html('Fungerar inte i prototyp')
          .addClass('mock-message-note')
          .fadeTo(3000, 0)
          .slideUp(500, function () {
            $(this).remove();
          });
      });
      $('.hsaHealthCareProviderShown').addClass('d-none');
    } else {
      $('#hsaHealthCareProviderDisclaimer001').remove();
      $('.hsaHealthCareProviderShown').removeClass('d-none');
    }
  });
  $('#hsaHealthCareUnit').click(function () {
    isCareUnitValue = $("input[name='hsaHealthCareUnit']").prop('checked');
    // $('#hsaGlnCode').prop('disabled', function (i, v) {
    //   return !v;
    // });
    // console.log('isCareUnitValue is ' + isCareUnitValue);
    if (isCareUnitValue == false) {
      $(this)
        .parent()
        .after(
          '<div class="alert alert-warning" id="hsaHealthCareUnitDisclaimer001" role="alert">\
          <p><i>Blekingesjukhuset Karlskrona</i> är INTE längre markerad som vårdenhet.</p>\
          <p>Är det en felaktig markering från början?</p>\
          <div class="custom-control custom-radio custom-control-inline">\
            <input type="radio" id="customRadioInline1" name="customRadioInline1" class="custom-control-input">\
            <label class="custom-control-label" for="customRadioInline1">Ja, det blev fel</label>\
          </div>\
          <div class="custom-control custom-radio custom-control-inline">\
            <input type="radio" id="customRadioInline2" name="customRadioInline2" class="custom-control-input">\
            <label class="custom-control-label" for="customRadioInline2">Nej, annan orsak</label>\
            </div>\
          </div>\
        '
        )
        .after(
          '<div class="alert mock-message-note" id="hsaHealthCareUnitDisclaimer002" role="alert">\
          <p>Prototypmeddelande som berör <i>Detta är en vårdenhet</i></p>\
          <p>Om användaren svarar nej - då presenteras varning <a href="#modal--disable-health-care-unit-provider" data-toggle="modal">enligt flöde</a>. Om användaren väljer arkivera - användaren anger datum då verksamheten upphör. Användaren får bekräftelse “Arkivering kommer att genomföras vid [angivet datum]”</p></div>\
        '
        );
      $('#hsaHealthCareUnitDisclaimer001').click(function () {
        $(this)
          .html('Fungerar inte i prototyp')
          .addClass('mock-message-note')
          .fadeTo(3000, 0)
          .slideUp(500, function () {
            $(this).remove();
          });
      });
      $('.hsaHealthCareUnitShown').addClass('d-none');
      $('#hsaGlnCode').parent().addClass('d-none');
    } else {
      $('#hsaHealthCareUnitDisclaimer001').remove();
      $('.hsaHealthCareUnitShown').removeClass('d-none');
      $('#hsaGlnCode').parent().removeClass('d-none');
      // $('#hsaGlnCodePadLock').remove();
    }
  });
  $('#editing-toggler-for-healthcare-unit-and-provider').click(function () {
    $(this).toggleClass('in-edit-mode');
    $('#hsaHealthCareProvider,#hsaHealthCareUnit').prop('disabled', function (i, v) {
      return !v;
    });
    if (
      $(this).is(".in-edit-mode")
    ) {
      $('#editing-toggler-for-healthcare-unit-and-provider').text('Nu är jag klar med ändring i vårdgivar-/vårdenhetsinformation');
    } else {
      $('#editing-toggler-for-healthcare-unit-and-provider').text('Jag behöver förändra vårdgivar-/vårdenhetsorganisationen');
    }
  });
});

// Detail view - check isCareUnit/hsaHealthCareUnit - STOP
